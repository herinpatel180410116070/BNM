import { CONSTANTS } from "./index";

export const

export const setProtectedData = (data) => {
  console.log("Calling: setProtectedData");
  return {
    type: CONSTANTS.SET_PROTECTED_DATA,
    payload: data,
  };
};

export const setNonProtectedData = (data) => {
  console.log("Calling: setNonProtectedData");
  return {
    type: CONSTANTS.SET_NONPROTECTED_DATA,
    payload: data,
  };
};

export const getNonProtectedRoute = () => {
  return apiAction({
    url: "/nonProtectedRoute",
    onSuccess: setNonProtectedData,
  });
};
export const getProtectedRoute = (token) => {
  return apiAction({
    url: "/protectedRoute",
    accessToken: token,
    onSuccess: setProtectedData,
  });
};

const apiAction = ({
  url = "",
  method = "GET",
  data = null,
  accessToken = null,
  onSuccess = () => { },
  onFailure = () => { },
  headersOverride = null,
}) => {
  return {
    type: CONSTANTS.API,
    payload: {
      url,
      method,
      data,
      accessToken,
      onSuccess,
      onFailure,
      headersOverride,
    },
  };
};
