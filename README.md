# BNM Project

This project contains:

- [BNM-Gateway]() which is a pre-configured [Spring Cloud Gateway]().
- [Keycloak]() that is being utilized as our Authentication and Authorization Service. **Note:** This can be found as a service under the projects [Docker-compose]() file.
- [Test Service]() which is a service that includes a [backend]() and [frontend]() to serve as an example for implementation, but also to test the project and ensure it is functioning.
- [Docker-Compose]() that allows the project to be run and tested, as well as serving as an example for implementation.
- [BNM_Market.json]() which is a pre-configured [realm]() for Keycloak and is utilized to build the project.


### Purpose

The BMN AuthSystem provides services for authenticating, authorizing, and managing users utilizing a SpringCloud Gateway and KeyCloak. Spring Cloud Gateway aims to provide a simple, yet effective way to route to APIs and provide cross cutting concerns to them such as: security, monitoring/metrics, and resiliency. For BNM it is used as a framework for routing users through the webpages. Keycloak is a open source software product used allow single sign-on with Identity and Access Management. Keycloak is used as graphical interface for managing the hosted BNM server.

### Install Instructions

#### MacOS

1. Install Docker for Mac https://docs.docker.com/desktop/mac/install/. There are two different possible installs, one for Intel Macs and one for M1 Macs. You can see which processor your mac has by clicking the Apple logo and selecting 'About this Mac'.
2. Once the installer is downloaded, double click the .dmg file and follow the on-screen installation instructions.
3. Open your Mac's terminal and navigate to a directory where you want to clone the BNM project (this is just a preference)
4. Once you are in the desired directory, run 'git clone https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/authsystem/BNM.git' to clone the project.
5. Run 'cd ./BNM' to navigate to the BNM folder.
6. Open docker-compose-prod.yaml and on line 7 underneath 'volumes:' change '//c/tmp:/tmp' to './tmp:/tmp'
7. Create a new folder named tmp in BNM
8. Move 'BNM/realm/BNM-test.json' to the tmp folder created in the previous step
9. Run 'docker-compose -f Docker-compose-prod.yml up' in the terminal you have open
10. At this point you will run into a File Not Found error. A possible fix is listed under the Current Status section below. However, even with this possible fix KeyCloak will fail at the end but all the containers will be initialized in Docker under a container named 'bnm'.

#### Windows
1. Install Docker - here is a helpful read me for that purpose https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/authsystem/iamsystem/-/blob/main/Docker-Instruction.md
2. Clone BNM system
3. Run the docker command 'docker-compose -f Docker-compose-prod.yml up' (Currently Fails - Runs correctly with our changes outlined below)
4. Run the docker command 'docker-compose -f Docker-compose-prod.yml up keycloak' (Runs with the docker-compose- File changes outlined below)
5. Navigate to http://localhost:8080/auth/
6. At this point the BNM project should be composed and the local keycloak server is accessible through the above port. 


### Current Status

Currently this is partially functional with the fixes listed below. These fixes have currently only been tested on MacOS but should work the same on Windows. These fixes are temporary and there will have to be permenant changes made in order for this to function properly.

#### KeyCloak Local Setup Instructions
1. Install Docker 
2. Clone BNM System Locally
3. Create a Folder to inside the Repo. We used the name tmp.
4. Copy the BNM-Test.json from realm folder and place it in tmp.
5. Open the docker-compose-prod.yml
    <br>
    5.1. Change the value of volume for the keycloak gateway to the path of BNM-Test.json. We used ./tmp:/temp (line 7)<br>
    5.2. Add the line ‘Ports: - 8080:8080’ below expose and before environment for the keycloakauthservice container (line 11 and 12)
6. Run command 'docker-compose -f Docker-compose-prod.yml up keycloak' Keycloak should compose correctly and can be seen running in docker.
7. Now that keycloak is running type 'http://localhost:8080/auth/' into your chosen browser. You should now have access to keycloak.

#### Fixes

**If on M1 Macs:** open 'Dockerfile.prod' located in './TestService/frontend/' and change line 2 from 'FROM node:12.2.0-alpine as build' to 'FROM node:12.4.0-alpine as build'. The following fixes will allow everything to run via Docker without errors, but version 8.0.1 of Keycloak is not compatible with M1 Macs.

1. Locate the reducers folder at './TestService/frontend/src/reducers/'
2. Duplicate the file named 'apiReducer.js' and call it 'apiReducerProtected.js'
3. Open the file called 'testApiActions.js' located in './TestService/frontend/src/actions/'
4. Delete line 3 'export const'
5. Open 'index.js' located in './TestService/frontend/src/reducers/' and on line 2 change 'import { apiReducer } from "./apiReducerProtected";' to 'import { apiReducerOpen } from "./apiReducerProtected";'
6. Open 'apiReducerProtected.js' in './TestService/frontend/src/reducers/' and change line 3 from 'export const apiReducer = (' to 'export const apiReducerOpen = ('
7. In 'docker-compose-prod.yaml' underneath line 10, add 'ports:' and underneath add '   - 8080:8080'
8. Run 'docker-compose -f Docker-compose-prod.yml up'

npm run build now runs successfully and you can access KeyCloak via the Docker Application


### Import/Export Realm

##### Export
```
docker exec -it KeycloakAuthService /opt/jboss/keycloak/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.realmName=BNM -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Dkeycloak.migration.file=/tmp/BNM-Test.json
```

##### Import
```
docker run -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT=/tmp/BNM-realm.json -v /tmp/example-realm.json:/tmp/BNM-realm.json jboss/keycloak
```
## Documentation

**_note to self_** talk about ssl/https: https://www.keycloak.org/docs/latest/server_installation/#enable-https-ssl-with-a-reverse-proxy and https://keycloak.discourse.group/t/keycloak-in-docker-behind-reverse-proxy/1195 talk about it

---

#### Librefoodpantry Documentation

Any documentation regarding the Libre food pantry can be found:

- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/StandardProject/)
- [Edit on GitLab](docs) (in `docs/` directory)

---

Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
